// event pada saat klik link


$('.page-scroll').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr("href");
    $("html").animate({
        scrollTop: $(href).offset().top - 55
    }, 1500, 'easeInOutExpo');



});

$('.nav ul li').click(function (e) {
    // console.log(e.currentTarget, "//", e.target, "//", this);
    e.stopPropagation();
    $('.nav ul li').removeClass('active');
    $(this).addClass('active');
});


